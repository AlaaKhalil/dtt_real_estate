//
//  HouseDetailViewController.swift
//  RealEstate
//
//  Created by Alaa Khalil on 05/08/2021.
//

import UIKit
import GoogleMaps


class HouseDetailViewController: UIViewController, GMSMapViewDelegate {
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var bathroomsLabel: UILabel!
    @IBOutlet weak var bedroomsLabel: UILabel!
    @IBOutlet weak var houseImage: UIImageView!
    @IBOutlet weak var descreptionTextView: UITextView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var detaillView: UIView!
    @IBOutlet weak var map: GMSMapView!
    var mapView: GMSMapView!
    var houseDetail: Houses?
    let locationManager = CLLocationManager()
    var distance: String?
    var houseLocation = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        self.map.addSubview(mapView)
        mapView.delegate = self
    }
    
    // Setting the data in the view
    func setData(){
        self.handlingMapView()
        self.sizeLabel.text = "\(houseDetail?.size ?? 0)"
        self.bathroomsLabel.text = "\(houseDetail?.bathrooms ?? 0)"
        self.bedroomsLabel.text = "\(houseDetail?.bedrooms ?? 0)"
        if !(UserDefaults.standard.bool(forKey: accessLocation)){
            self.distanceView.isHidden = true
        }
        self.distanceLabel.text = "\(self.distance ?? "") km"
        self.descreptionTextView.text = houseDetail?.description
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 0;
        self.priceLabel.text = formatter.string(from: (houseDetail?.price ?? 0) as NSNumber)
        if let value = houseDetail?.image {
            if let url = URL(string: (baseUrl + value)) {
                self.houseImage.setImageWithURL(url: url, placeHolderImage: nil)
            }
        }
        else {
            self.houseImage.image = nil
        }
    }
    
    // Show the house location and add marker for it
    func handlingMapView() {
        let camera = GMSCameraPosition.camera(withLatitude: Double((houseDetail?.latitude)!), longitude: Double((houseDetail?.longitude)!), zoom: 15.0)
        mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        let position = CLLocationCoordinate2D(latitude: Double((houseDetail?.latitude)!), longitude: Double((houseDetail?.longitude)!))
        let marker = GMSMarker(position: position)
        marker.title = "\(houseDetail?.zip ?? " ")\(houseDetail?.city ?? " ")"
        marker.map = self.mapView
    }
    
    // Go to pervious HomeViewController
    @IBAction func dismissPressedButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // Adding RoundCorners to the view
        detaillView.RoundCorners(corners: [.topRight,.topLeft], radius: 20.0)
    }
    override func viewDidAppear(_ animated: Bool) {
        mapView.frame = map.bounds
    }
}

// MARK: - CLLocationManagerDelegate

extension HouseDetailViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager,didChangeAuthorization status: CLAuthorizationStatus){
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.requestLocation()
        map.isMyLocationEnabled = true
        map.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager,didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        map.camera = GMSCameraPosition(target: location.coordinate,zoom: 15,bearing: 0,viewingAngle:0)
    }
    
    func locationManager(_ manager: CLLocationManager,didFailWithError error: Error){
        print(error)
    }
}



