    //
    //  InformationViewController.swift
    //  RealEstate
    //
    //  Created by Alaa Khalil on 06/08/2021.
    //
    
    import UIKit
    
    class InformationViewController: UIViewController {
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Do any additional setup after loading the view.
        }
        //Go to the DTT website
        @IBAction func urlPressedButton(_ sender: Any) {
            if let url = URL(string: "https://www.d-tt.nl/"),
               UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:])
            }
        }
    }
