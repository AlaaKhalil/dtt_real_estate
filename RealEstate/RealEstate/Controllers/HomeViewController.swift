//
//  HomeViewController.swift
//  RealEstate
//
//  Created by Alaa Khalil on 04/08/2021.
//

import UIKit
import GoogleMaps
import CoreLocation


class HomeViewController: UIViewController {
    @IBOutlet weak var housePlaceholder: UIView!
    @IBOutlet weak var houseTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var houses = [Houses]()
    var sortedHouses = [Houses]()
    var searchedHouses = [Houses]()
    var searching = false
    let locationManager = CLLocationManager()
    var userCoordinate:CLLocation?
    var distances = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.housePlaceholder.isHidden = true
        self.adjustmentSearchBar()
        self.getCurrentLocation()
        if UserDefaults.standard.object(forKey: accessLocation) != nil && !UserDefaults.standard.bool(forKey: accessLocation){
            self.getHouses()
        }
    }
    
    // Customize searchBar to match the design
    func adjustmentSearchBar(){
        self.searchBar.delegate = self
        searchBar.backgroundImage = UIImage()
        let searchTextField:UITextField = searchBar.value(forKey: "searchField") as? UITextField ?? UITextField()
        searchTextField.layer.cornerRadius = 15
        searchTextField.font = UIFont(name: "GothamSSm-Light", size: 12.0)
    }
    
    
    func getCurrentLocation(){
        if (CLLocationManager.locationServicesEnabled()){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    // Fetching the data from Api
    func getHouses(){
        ServerManger.shared.getHouses{ response, error in
            if error == nil{
                self.houses = response
                self.sortedHouses = response.sorted { (first, second) -> Bool in
                    return first.price! < second.price!
                }
            }
            self.setData()
        }
    }
    
    // Handling the searchBar results to set the data in tableView
    func setData(){
        self.houseTableView.reloadData()
        if self.searching{
            if self.searchedHouses.isEmpty{
                self.houseTableView.isHidden = true
                self.housePlaceholder.isHidden = false
            }
            else{
                self.houseTableView.isHidden = false
                self.housePlaceholder.isHidden = true
            }
        }
        else{
            self.houseTableView.isHidden = false
            self.housePlaceholder.isHidden = true
        }
    }
    
    // Dismiss the keyboard by touching anywhere on the screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

// MARK: - UITableViewDelegate
extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return self.searchedHouses.count
        } else {
            return self.sortedHouses.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = houseTableView.dequeueReusableCell(withIdentifier: "HomeCell") as! HomeCell
        var houseObject = self.sortedHouses[indexPath.row]
        if searching{
            houseObject = self.searchedHouses[indexPath.row]
        }
        self.distances["\(houseObject.id ?? 0)"] = cell.setData(house: houseObject, userCoordinate: self.userCoordinate)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "HouseDetail", bundle: nil).instantiateViewController(withIdentifier: "HouseDetailController") as! HouseDetailViewController
        var selectedHouse = self.sortedHouses[indexPath.row]
        if searching{
            selectedHouse = self.searchedHouses[indexPath.row]
        }
        let selectedId = selectedHouse.id ?? 0
        vc.distance = distances["\(selectedId)"]
        self.searchBar.searchTextField.endEditing(true)
        vc.houseDetail = selectedHouse
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
}

// MARK: - UISearchBarDelegate
extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedHouses = sortedHouses.filter({$0.zip!.replacingOccurrences(of: " ", with: "", options: .regularExpression) .lowercased().contains(searchText.replacingOccurrences(of: " ", with: "", options: .regularExpression).trimmingCharacters(in: .whitespacesAndNewlines).lowercased()) || $0.city!.trimmingCharacters(in: .whitespacesAndNewlines).lowercased().contains(searchText.trimmingCharacters(in: .whitespacesAndNewlines).lowercased())})
        if searchText.isEmpty{
            searching = false
        }
        else{
            searching = true
        }
        self.setData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        self.setData()
    }
}

// MARK: - CLLocationManagerDelegate
extension HomeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager:CLLocationManager,didUpdateLocations locations: [CLLocation]){
        self.userCoordinate = locations.last
        self.getHouses()
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.locationServicesEnabled() {
            switch locationManager.authorizationStatus {
            case .notDetermined, .restricted, .denied:
                self.getHouses()
                UserDefaults.standard.set(false, forKey: accessLocation)
            case .authorizedAlways, .authorizedWhenInUse:
                UserDefaults.standard.set(true, forKey: accessLocation)
                locationManager.startUpdatingLocation()
            @unknown default:
                break
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager,didFailWithError error: Error){
       
        print(error)
    }
}



