//
//  HomeCell.swift
//  RealEstate
//
//  Created by Alaa Khalil on 04/08/2021.
//

import UIKit
import GoogleMaps

class HomeCell: UITableViewCell {
    
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var houseImage: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var bathroomsLabel: UILabel!
    @IBOutlet weak var bedroomsLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // setting the house data from Api in the house Cell
    func setData(house: Houses, userCoordinate: CLLocation?)-> String{
        self.sizeLabel.text = "\(house.size ?? 0)"
        self.addressLabel.text = (house.zip ?? " ") + " " + (house.city ?? " ")
        self.bathroomsLabel.text = "\(house.bathrooms ?? 0)"
        self.bedroomsLabel.text = "\(house.bedrooms ?? 0)"
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 0;
        self.priceLabel.text = formatter.string(from: (house.price ?? 0) as NSNumber)
        
        //setting the image of house
        if let value = house.image {
            if let url = URL(string: (baseUrl + value)) {
                self.houseImage.setImageWithURL(url: url, placeHolderImage: nil)
            }
        }else {
            self.houseImage.image = nil
        }
        // check the location access and determine the distance between current location and house location
        if UserDefaults.standard.bool(forKey: accessLocation){
            self.distanceView.isHidden = false
            let houseCoordinate = CLLocation(latitude: Double(house.latitude ?? 0), longitude: Double(house.longitude ?? 0))
            let distance = ((userCoordinate?.distance(from:houseCoordinate))!)/1000
            let formattedDistance = String(format: "%.1f", distance)
            self.distanceLabel.text = "\(formattedDistance) Km"
            return formattedDistance
        }
        self.distanceView.isHidden = true
        return ""
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
