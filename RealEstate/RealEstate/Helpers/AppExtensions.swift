//
//  AppExtensions.swift
//  RealEstate
//
//  Created by Alaa Khalil on 04/08/2021.
//

import Foundation
import UIKit
import Kingfisher

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    func RoundCorners(corners:UIRectCorner, radius: CGFloat)
    {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        self.layer.mask = maskLayer
    }
}

extension UIImageView {
    func setImageWithURL(url: URL, placeHolderImage placeholder: UIImage?) {
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url,
                         placeholder: placeholder,
                         options: [.transition(.fade(1))],
                         progressBlock: nil) { (result) in
                            switch result {
                            case .success(let value):
                                self.image = value.image
                            case .failure(let error):
                                print("KingFisher Job failed: \(error.localizedDescription)")
                            }
        }
    }
}
